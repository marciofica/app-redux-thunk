import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { loadDataAction } from '../actions'
const Info = ({loadData, data, isFetching, error}) => {
    
    useEffect(() => {
        loadData()
    }, [loadData])

    if(isFetching) return <i className="fa-2x fas fa-atom fa-spin"></i>
    if(error) return <span>Aconteceu algum problema!</span>

    return (
        <span>Info: {data.origin}</span>
    )
}

const mapStateToProps = (state) => {
    return {
        data: state.data,
        isFetching: state.isFetching,
        error: state.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadData: () => dispatch(loadDataAction())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Info)
