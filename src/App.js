import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import reducerDefault from './reducers'
import Info from './components/Info'

let store = createStore(reducerDefault, applyMiddleware(thunk, logger))

const App = () => {
  return (
    <Provider store={store}>
      <div className="container">
        <Info />
      </div>
    </Provider>
  )
}

export default App
