import axios from "axios"

const loadDataFetchAction = () => {
    return {
        type: 'LOAD_DATA_FETCH'
    }
}

const loadDataSuccessAction = (data) => {
    return {
        type: 'LOAD_DATA_SUCCESS',
        data
    }
}
const loadDataErrorAction = (data) => {
    return {
        type: 'LOAD_DATA_ERROR'
    }
}

export const loadDataAction = () => {
    return dispatch => {
        dispatch(loadDataFetchAction())
        axios
            .get('http://httpbin.org/ip')
            .then(data => {
                dispatch(loadDataSuccessAction(data.data))
            })
            .catch(() => {
                dispatch(loadDataErrorAction())
            })
    }
}