const INITIAL_STATE = {
    data: [],
    isFetching: false,
    error: false
}

const reducerDefault = (state=INITIAL_STATE, action) => {
    switch (action.type) {
        case 'LOAD_DATA_FETCH':
            return {...state, isFetching: true, error: false}
        case 'LOAD_DATA_SUCCESS':
            return {...state, isFetching: false, data: action.data, error: false}
        case 'LOAD_DATA_ERROR':
            return {...state, isFetching: false, data: [], error: true}
        default:
            return state
    }
}

export default reducerDefault
